#ifndef __PROGTEST__
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <vector>
#include <array>
#include <cassert>
using namespace std;
#endif /* __PROGTEST__ */

struct Person{
string name;
string surrname;
string telnum;
};



bool Parse_String(string & line, Person & DATA,string & end ){
DATA.telnum = -1; 
istringstream stream(line);
stream >> DATA.name >> DATA.surrname >> DATA.telnum >> end;



return true;
}

bool Check_Validity(const Person & DATA ) {
  
  for(size_t i=0; i < DATA.telnum.size() ; i++)
  if(!isdigit(DATA.telnum[i])) return false;
  if(DATA.telnum.size()!=9) return false;
  if(DATA.telnum[0] == '0' ) return false;

return true;
}

int Find(const vector<Person> & DATA, const string & query,ostream & out)
{
  int count = 0;
for(size_t i=0;i<DATA.size();i++){
if(DATA[i].name == query || DATA[i].surrname == query)
{count++ ;
out << DATA[i].name << " " << DATA[i].surrname << " " <<DATA[i].telnum<<endl;
}
}
return count;
}


bool report ( const string & fileName, ostream & out )
{

  ifstream ifs;
  string line;
  vector<Person> Data;
  Person Element;
  string end = "";

ifs.open(fileName,ifstream::in);
if(ifs.is_open()){
while(getline(ifs,line)){
if(line==end) break;
if(!Parse_String(line,Element,end)) break;
if(end!="") return false;
if(!Check_Validity(Element)) return false;
Data.push_back(Element);
}

while(getline(ifs,line))
{
  string query, trash= "" ;
istringstream stream(line);
stream >> query >> trash;
if( trash != "" ) return false;
int x=( Find ( Data, query,out ));
 out  << "-> " << x << endl; 
}
return true;

}

else return false;
}

#ifndef __PROGTEST__
int main ()
{
  ostringstream oss;
  oss . str ( "" );
  assert ( report( "tests/test0_in.txt", oss ) == true );
  assert ( oss . str () ==
    "John Christescu 258452362\n"
    "John Harmson 861647702\n"
    "-> 2\n"
    "-> 0\n"
    "Josh Dakhov 264112084\n"
    "Dakhov Speechley 865216101\n"
    "-> 2\n"
    "John Harmson 861647702\n"
    "-> 1\n" );
  oss . str ( "" );
  assert ( report( "tests/test1_in.txt", oss ) == false );
  return 0;
}
#endif /* __PROGTEST__ */
