#ifndef __PROGTEST__
#include <cassert>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
using namespace std;
#endif /* __PROGTEST__ */

struct File
  {
    unsigned char * data;
    uint32_t position;
    uint32_t size;
    File(void) : data(nullptr), position(0), size(0) {};

    File  operator = ( const File & prev)
    {
      if(prev.size == 0 ){
        size = 0;
        position = 0;
        data = nullptr;
        return *this;}

      data = new unsigned char [prev.size ];
      memcpy(data, prev.data, prev.size );
      position = prev.position;
      size = prev.size;
      
      return *this;
    }
};

class CFile
{
public:
CFile( void ) : Files(nullptr), version(-1){};
CFile( const CFile & Files);
~CFile()
  {
    for(int32_t i = 0 ; i <= version ; i ++)
    delete [] Files[i].data;
    delete  [] Files ;
  }
bool seek ( uint32_t offset );
uint32_t read ( uint8_t * dst, uint32_t bytes );
uint32_t write ( const uint8_t * src,uint32_t bytes );
void truncate ( void );
uint32_t fileSize ( void ) const;
void addVersion ( void );
bool undoVersion ( void );
bool checksource(const uint8_t * file, uint32_t bytes);
void checksize(uint32_t bytes);
void writedata(const uint8_t * src, uint32_t bytes);
void fillempty();
uint32_t readsize(uint32_t bytes);
void printit();
CFile & operator = (const CFile & a)
{

for(auto i = 0 ; i < version + 1 ; i++ )
delete [] Files[i].data;
delete [] Files;
version = a.version;

if(a.Files == nullptr ){
Files = nullptr;
return *this;}

Files = new File [a.version + 1 ];
for(int32_t i = 0 ; i < a.version + 1 ; i ++ )
{
if(a.Files[i].size == 0 )
  {
    Files[i].data = nullptr;
    Files[i].position = a.Files[i].position;
    Files[i].size = a.Files[i].size;
    continue;
  }

Files[i].data = new unsigned char [a.Files[i].size];
Files[i].position = a.Files[i].position;
Files[i].size = a.Files[i].size;

for(uint32_t k = 0 ; k < a.Files[i].size ; k ++ )
Files[i].data[k] = a.Files[i].data[k];
}

return *this;
};

private:
File * Files;
int32_t version;
};

bool CFile::undoVersion ( void )
{
if(version == 0 || version == -1 ) return false;
delete [] Files[version].data;
version--;
return true;
}


void CFile::truncate ( void )
{
  if(Files == nullptr ) return;
  auto NewArr =  new unsigned char [ Files[version].position ];
  memcpy(NewArr, Files[version].data,  Files[version].position );
  delete[] Files[version].data;
  Files[version].data = NewArr;
  Files[version].size = Files[version].position ;
}

  CFile::CFile( const CFile & Orig)
  {
  version = Orig.version;
  //if there is no version
  if(version == -1 ){
    Files = nullptr;
    return;}

  Files = new File [Orig.version + 1 ];
  for(int32_t i = 0 ; i < Orig.version + 1  ; i++ )
      Files[i] = Orig.Files[i] ;
  }

  uint32_t CFile::readsize(uint32_t bytes)
  {
      if(Files[version].size < Files[version].position + bytes ) 
        return Files[version].size - Files[version].position;
        //fuction to return readable bytes from demand
      return bytes;
  }

  bool CFile::seek ( uint32_t offset )
  {
  if(Files == nullptr && offset == 0 ) return true;
  if(Files == nullptr ) return false;
  if(offset > Files[version].size || offset < 0  ) return false; // offset out of file 
  Files[version].position = offset;
  if(Files[version].data == nullptr ) return false;
  
  return true;
  }

  void CFile ::fillempty()
  {
    Files[version].data = nullptr;
    Files[version].position = 0;
    Files[version].size = 0;
  }

  bool CFile::checksource(const uint8_t * file, uint32_t bytes)
  { return (file == nullptr || bytes == 0) ;}

  void CFile::checksize(uint32_t bytes)
  {
    //function to resize array
  uint32_t NewSize = Files[version].position + bytes;
  if( Files[version].size < NewSize ){
  auto NewArr = new unsigned char [NewSize];
  memcpy(NewArr, Files[version].data, Files[version].size );
  delete [] Files[version].data;
  Files[version].data = NewArr;
  Files[version].size = NewSize;
  }
  }

  void CFile::writedata(const uint8_t * src, uint32_t bytes)
  {
    for(uint32_t i = 0 ; i < bytes ; i++ )
    {
      Files[version].data[Files[version].position] = src[i];
      Files[version].position += 1;
    }
  }

  uint32_t CFile::fileSize ( void ) const
  { 
    if(Files == nullptr) return 0;
    return Files[version].size ;
  
  }

  void CFile::addVersion ( void )
  {
    auto NewArr = new File [(++version + 1)];
    if(version == 0 ){
    Files = NewArr;
    fillempty();
    return;
    }
     for(auto i = 0 ; i < version  ; i ++ )
     {
      NewArr[i].data = new unsigned char [Files[i].size];
      memcpy(NewArr[i].data, Files[i].data, Files[i].size);
      NewArr[i].size = Files[i].size;
      NewArr[i].position = Files[i].position;
      delete [] Files[i].data;
    }

    delete [] Files;
    Files = NewArr;

    int32_t curr = version, prev = version - 1 ;
    Files[curr] = Files[prev];
    return;
  }

  uint32_t CFile::write ( const uint8_t * src,uint32_t bytes )
  {
    if( checksource(src,bytes) ) return false;
    if(version == -1 ) this->addVersion();
    checksize(bytes);
    writedata(src, bytes);
    return bytes;
  }

  uint32_t CFile::read ( uint8_t * dst, uint32_t bytes )
  {
    if(Files == nullptr ) return 0;
    if( checksource(dst,bytes) || Files[version].size == 0  ) return false;
    auto ToRead = readsize(bytes);
    for(uint32_t i = 0 ; i < ToRead ; i ++ )
    dst[i] = Files[version].data[ Files[version].position + i ];
    Files[version].position += ToRead;
    return ToRead;
  }

#ifndef __PROGTEST__
bool writeTest ( CFile & x, const initializer_list<uint8_t> & data, uint32_t          wrLen )
{
  return x . write ( data . begin (), data . size () ) == wrLen;
}

bool readTest ( CFile & x, const initializer_list<uint8_t> & data, uint32_t rdLen )
{
  uint8_t  tmp[100];
  uint32_t idx = 0;

  if ( x . read ( tmp, rdLen ) != data . size ())
    return false;
  for ( auto v : data )
    if ( tmp[idx++] != v )
    return false;
  return true;
}

int main ( void )
{
  CFile f0;
  
  CFile fa;

  writeTest ( fa, { 10, 20, 30 }, 3 );
  fa.seek(0);

  CFile fb;
  //writeTest ( fb, { 11, 21, 31 }, 3 );

  fb = fa;
  fb.seek(0);



  fa.fileSize();
  writeTest ( fa, { 10, 20, 30 }, 3 );
 readTest ( fa, { 10, 20, 30 }, 3 );



  f0.seek(3);
  assert ( writeTest ( f0, { 10, 20, 30 }, 3 ) );
  assert ( f0 . fileSize () == 3 );
  assert ( writeTest ( f0, { 60, 70, 80 }, 3 ) );
  assert ( f0 . fileSize () == 6 );
  assert ( f0 . seek ( 2 ));
  assert ( writeTest ( f0, { 5, 4 }, 2 ) );
  assert ( f0 . fileSize () == 6 );
  assert ( f0 . seek ( 1 ));
  assert ( readTest ( f0, { 20, 5, 4, 70, 80 }, 7 ));
  assert ( f0 . seek ( 3 ));
  //f0.seek(100);


  f0 . addVersion();
  assert ( f0 . seek ( 6 ));
  assert ( writeTest ( f0, { 100, 101, 102, 103 }, 4 ) );
  f0 . addVersion();
  assert ( f0 . seek ( 5 ));
  CFile f1 ( f0 );
  f0 . truncate ();
  assert ( f0 . seek ( 0 ));
  //cout << f0.fileSize() << endl;
  assert ( readTest ( f0, { 10, 20, 5, 4, 70 }, 20 ));
  assert ( f0 . undoVersion () );
  assert ( f0 . seek ( 0 ));
  assert ( readTest ( f0, { 10, 20, 5, 4, 70, 80, 100, 101, 102, 103 }, 20 ));
  assert ( f0 . undoVersion () );
  assert ( f0 . seek ( 0 ));
  assert ( readTest ( f0, { 10, 20, 5, 4, 70, 80 }, 20 ));
  assert ( !f0 . seek ( 100 ));
  assert ( writeTest ( f1, { 200, 210, 220 }, 3 ) );
  assert ( f1 . seek ( 0 ));
  assert ( readTest ( f1, { 10, 20, 5, 4, 70, 200, 210, 220, 102, 103 }, 20 ));
  assert ( f1 . undoVersion () );
  assert ( f1 . undoVersion () );
  assert ( readTest ( f1, { 4, 70, 80 }, 20 ));
  assert ( !f1 . undoVersion () );
  return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */
