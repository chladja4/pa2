#ifndef __PROGTEST__
#include <cstring>
#include <iostream>
using namespace std;
class CLinkedSetTester;
//#include <iostream>
#endif /* __PROGTEST__ */
class CLinkedSet
{
private:
    struct CNode
    {
        CNode * m_Next;
        char * value;
        const char * Value () const
        {
            return value;
        }
        CNode() : m_Next(nullptr) , value(nullptr) {};
    };
    CNode * m_Begin;
    size_t m_Size;
public:
    CLinkedSet() : m_Begin(nullptr), m_Size(0){};

    CLinkedSet(const CLinkedSet & a){

    if(a.m_Begin == nullptr)
    {
    m_Begin = nullptr;
    m_Size = 0;        
    return;
    }
    //------------------------ empty object
    CNode * current = a.m_Begin;
    CNode * copy = new CNode;
    CNode * headcopy = copy; 
    CNode * prev = nullptr;
    while(current != nullptr)
    {
        size_t len = strlen(current->value) + 1 ;
        copy->value = new char[len];
        strncpy(copy->value, current->value, len);
        if( current ->m_Next != nullptr )
            {
                copy -> m_Next = new CNode;
                copy = copy -> m_Next;
            }
    //--------------------- if its not last node create new node
        prev = current;
        current = current->m_Next;
    }
    prev->m_Next = nullptr;
    m_Begin = headcopy;
    m_Size = a.Size();
    };

    CLinkedSet & operator = ( const CLinkedSet & a)
    {
        if(a.m_Begin == m_Begin) return *this;
        //^---------------------------- same objects
        CLinkedSet copy (a);
        auto ToDel = m_Begin;
        m_Begin = copy.m_Begin;
        copy.m_Begin = ToDel;
        m_Size = a.Size();

        //swap begin with copy begin, on copy is called destructror
        return *this;
    };

    ~CLinkedSet(){
        while( m_Begin != nullptr )
        {
            auto tmp = m_Begin;
            delete [] tmp->value;
            m_Begin = m_Begin->m_Next;
            delete tmp;
        }
    };

    CNode * GetNode(const char * value)
    {
        CNode * NewNode = new CNode;
        size_t len = strlen(value) + 1 ;
        NewNode->value = new char[len];
        strncpy(NewNode->value, value, len);
        NewNode->m_Next = nullptr;     
        return NewNode;
    };

    bool Insert ( const char * value ){
    if(m_Begin == nullptr)
    {
       m_Begin = GetNode(value);
       m_Begin ->m_Next = nullptr;
       m_Size++;
       return true;
    }
    //^---------------------------- insert into empty list
    if( m_Begin != nullptr && strcmp( m_Begin->value , value) == 0 )  return false;
    //^---------------------------- found the same value at head
    if(strcmp(value, m_Begin->value) < 0 )
        {
           CNode * NewNode = GetNode( value );
           NewNode->m_Next = m_Begin;
           m_Begin = NewNode;
           m_Size++;
           return true;
        } 
    //^----------------------------- value is less than head insert as new head 
        CNode * current = m_Begin;
        CNode * prev = nullptr;
        while(current != nullptr )
        {
            if(strcmp( current->value ,value) == 0 )  return false;
            if(strcmp ( current->value, value ) > 0) break;
            prev = current;
            current = current->m_Next;
        }
    //^----------------------------- going through, finding value higher than, taking note of prev node
        auto NewNode = GetNode(value);
        m_Size++;
        if(current == nullptr)
        {
            prev->m_Next = NewNode;
            NewNode ->m_Next = nullptr;
            return true;
        }
    //^----------------------------- The place for node is at the end of LL 
        auto tmp = prev->m_Next;
        prev->m_Next = NewNode;
        NewNode -> m_Next = tmp ;
        return true;
    };
    
    
    bool Remove ( const char * value )
    {
        if(m_Begin == nullptr) return false;
        //^----------------------------- empty LL 
        CNode * current = m_Begin;
        if(strcmp(m_Begin->value ,value) ==  0)
        {
            m_Size--;
            m_Begin = m_Begin->m_Next;
            delete [] current->value;
            delete current;
            return true;
        }
        //^----------------------------- Searched value at head node
        if(current->m_Next == nullptr && strcmp(m_Begin->value ,value) ==  0)
        {
            delete [] current->value;
            delete current;
            m_Begin = nullptr;
            m_Begin->m_Next = nullptr;
            m_Size--;
            return true;
        }
        //^----------------------------- One node at LL 
            CNode * prev = m_Begin;
            while(current != nullptr ) 
            {
                if( strcmp ( current->value ,value) ==  0) break;
                prev = current;
                current = current -> m_Next ; 
            }
        //^----------------------------- going through trying to find the VALUE
            if(current == nullptr) return false;
        //^----------------------------- not found
            m_Size--;
            if(current -> m_Next == nullptr)
            {
                prev->m_Next = nullptr;
                delete [] current->value;
                delete current;
                return true;
            }
        //^----------------------------- the node to remove is the last node
        
            prev->m_Next = current->m_Next;
            current->m_Next = nullptr;
            delete [] current->value;
            delete current;
            return true;
        //^----------------------------- pointing over the node to delete, make sure the current -> next is null
    };

    bool Empty () const
    { return m_Begin == nullptr ;};

    size_t Size () const
    { return m_Size;};

    bool Contains ( const char * value ) const
    {
        CNode * current = m_Begin;
        while(current != nullptr)
        {
            if( strcmp(current->value ,value) == 0 ) return true;
                current = current ->m_Next;
        }
        return false;
    };

    // void print(){
    //     auto current = m_Begin;
    //     while(current!= nullptr)
    //     {
    //         cout << current -> value;
    //         current = current->m_Next;
    //     }
    // }

    friend class ::CLinkedSetTester;
};

#ifndef __PROGTEST__
#include <cassert>

struct CLinkedSetTester
{
    static void test0()
    {
        CLinkedSet x0;
        assert( x0 . Insert( "Jerabek Michal" ) );
        assert( x0 . Insert( "Pavlik Ales" ) );
        assert( x0 . Insert( "Dusek Zikmund" ) );
        assert( x0 . Size() == 3 );
        //x0.print();
        assert( x0 . Contains( "Dusek Zikmund" ) );
        assert( !x0 . Contains( "Pavlik Jan" ) );
        assert( x0 . Remove( "Jerabek Michal" ) );
        assert( !x0 . Remove( "Pavlik Jan" ) );
        assert( !x0 . Empty() );
    }

    static void test1()
    {
        CLinkedSet x0;
        assert( x0 . Insert( "Jerabek Michal" ) );
        assert( x0 . Insert( "Pavlik Ales" ) );
        assert( x0 . Insert( "Dusek Zikmund" ) );
        assert( x0 . Size() == 3 );
        assert( x0 . Contains( "Dusek Zikmund" ) );
        assert( !x0 . Contains( "Pavlik Jan" ) );
        assert( x0 . Remove( "Jerabek Michal" ) );
        assert( !x0 . Remove( "Pavlik Jan" ) );
        CLinkedSet x1 ( x0 );
        assert( x0 . Insert( "Vodickova Saskie" ) );
        assert( x1 . Insert( "Kadlecova Kvetslava" ) );
        assert( x0 . Size() == 3 );
        assert( x1 . Size() == 3 );
        assert( x0 . Contains( "Vodickova Saskie" ) );
        assert( !x1 . Contains( "Vodickova Saskie" ) );
        assert( !x0 . Contains( "Kadlecova Kvetslava" ) );
        assert( x1 . Contains( "Kadlecova Kvetslava" ) );
        CLinkedSet x3 ( x1 );
        CLinkedSet x4 ( x3 );
        CLinkedSet x5 ( x3 );
        CLinkedSet x6 ( x1 );
        CLinkedSet x7 ( x5 );
        x3 = x1;
        x1 = x3;
        x3 = x1;
        x3.Size();
        x3.Empty();
        x3.Contains("Kadlecova Kvetslava");
        x3.Contains("Kadlecova Kaproslava");


    }

    static void test2()
    {
        CLinkedSet x0;
        CLinkedSet x1;
        assert( x0 . Insert( "Jerabek Michal" ) );
        assert( x0 . Insert( "Pavlik Ales" ) );
        assert( x0 . Insert( "Dusek Zikmund" ) );
        assert( x0 . Size() == 3 );
        assert( x0 . Contains( "Dusek Zikmund" ) );
        assert( !x0 . Contains( "Pavlik Jan" ) );
        assert( x0 . Remove( "Jerabek Michal" ) );
        assert( !x0 . Remove( "Pavlik Jan" ) );
        x1 = x0;
        assert( x0 . Insert( "Vodickova Saskie" ) );
        assert( x1 . Insert( "Kadlecova Kvetslava" ) );
        assert( x0 . Size() == 3 );
        assert( x1 . Size() == 3 );
        assert( x0 . Contains( "Vodickova Saskie" ) );
        assert( !x1 . Contains( "Vodickova Saskie" ) );
        assert( !x0 . Contains( "Kadlecova Kvetslava" ) );
        assert( x1 . Contains( "Kadlecova Kvetslava" ) );
        //----------------
     
    }

};

int main ()
{
    CLinkedSetTester::test0();
   CLinkedSetTester::test1();
   CLinkedSetTester::test2();

    return 0;
}
#endif /* __PROGTEST__ */
