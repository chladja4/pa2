#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cassert>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
#include <memory>
#include <functional>
#include <stdexcept>
using namespace std;
#endif /* __PROGTEST__ */
using namespace std;
struct Node
{
  string letter;
  Node * right ;
  Node * left ;
  Node();
};


class Huffdecom
{
public:
Huffdecom ( unsigned long ByteSize, char * Data , const char * outFileName);
~Huffdecom();
bool GrowTree();
bool Decode();

private:
unsigned int count = 0 ;
unsigned int LettersToFind = 0;
int Part = 0;
unsigned long ByteSize;
unsigned long BitPos = 8;
unsigned long BytePos = -1;
Node * Tree = nullptr;
bool TreeSucces = 1;
const char * outFileName;
char * Data = nullptr;
unsigned int Fail = 0;
int OneByte [8];
Node * RecbuildSubtree();
bool MoveToNextBit();
char  GetAsciiLetter();
void ChopTree( Node * CurrentNode );
bool readData( const char * inFileName );
bool LoadChunk( bool type , ostream & ofs);
};

Node::Node()
{
  left=right=nullptr;
}

Huffdecom::Huffdecom ( unsigned long  ByteSize, char * Data , const char * outFileName ) :  ByteSize(ByteSize)
{
this -> outFileName = outFileName;
this->Data = Data; 
Tree = nullptr;
}

Huffdecom::~Huffdecom()
{
  ChopTree(Tree);
  delete [] Data;
}

void Huffdecom::ChopTree(Node * CurrentNode)
{
  if(CurrentNode != nullptr)
  {
    ChopTree( CurrentNode->left );
    ChopTree( CurrentNode->right );

    delete CurrentNode;   
  }
  
}

bool Huffdecom::MoveToNextBit ()
{
  if(BitPos > 7)
  {
    BytePos++;
    if ( ByteSize == BytePos  ) {
    Fail = 1;
    return false;
    }
    Part = Data[BytePos];
    for( int i = 0 ; i < 8 ; i++)
    OneByte[i] = (Part >> (7 - i) ) % 2;
    BitPos=0;
  }
  return OneByte[BitPos++];
}

Node * Huffdecom::RecbuildSubtree()
{
  if(ByteSize == BytePos)
  {
     Fail=1;
     return nullptr;
  }
bool bit = MoveToNextBit();
if(bit == 0 )
  {
    Node * CurrentNode  = new Node ();
    CurrentNode -> left = RecbuildSubtree();
    //if something fails recursion will delete node and return nullptr to delete other nodes 
  if(CurrentNode->left == nullptr ){
      delete CurrentNode;
      return nullptr;
    }

    CurrentNode -> right = RecbuildSubtree();
    if( CurrentNode -> right == nullptr){
      delete CurrentNode;
      return nullptr;
    }
    return CurrentNode; 
  }

  else
  {
    Node * leaf = new Node();
    leaf -> letter = GetAsciiLetter();

    if(leaf -> letter.empty()){
      Fail=1;
      delete leaf;
      return nullptr;  
    }
    return leaf;
  }
}

bool Huffdecom::GrowTree()
{
  Tree = RecbuildSubtree();
  if ( !TreeSucces || Tree == nullptr ||Fail )
    {
      ChopTree(Tree);
      return false;
    }
  return true;
}

char Huffdecom::GetAsciiLetter()
{
  int Byte[8];
  for ( int i = 0 ; i < 8 ; i ++ )
  Byte[i] = MoveToNextBit();
  if( Byte[0] != 0 ) Fail=1;
  int letterNum = Byte[0] * 128 + Byte[1] * 64 + Byte[2] * 32 + Byte[3] * 16 + Byte[4] * 8 + Byte[5] * 4 + Byte[6] * 2 + Byte[7] * 1; 
  char letter = letterNum;
  return letter;
}

int BinaryToInt ( int * Array, unsigned int size)
{
  int transferred = 0, position=1;
for(unsigned i = 0 ; i < size ; i++ )
  {
     transferred += Array[i] * 2048  / position ;
     position *=2; 
  }
  return transferred;
}

bool Huffdecom::LoadChunk( bool type, ostream & ofs )
{
  count = 0 ;
  Node * head = Tree;
  int Size[12],currentbit;
  if(type == 1)
  {
    for(;count < 4096 ;)
        {
          if( !head->letter.empty() )
              {
                ofs << head->letter;
                if(ofs.fail()) return false;
                head = Tree;
                count++;
                continue;
              } 
          currentbit = MoveToNextBit();
          if( Fail ) return false;
          if (currentbit == 0 ) head = head-> left;
          else head = head->right ;
        }
  }
  else 
  {
  for( int i = 0 ; i < 12 ; i++)
      Size[i] = MoveToNextBit();
       LettersToFind = BinaryToInt(Size, sizeof(Size) / sizeof(int) );

    for(;count < LettersToFind;)
      {
        if( !head->letter.empty() )
              {
                ofs << head->letter;
                if(ofs.fail()) return false;
                head = Tree;
                count++;
                continue;
              }
            
        currentbit = MoveToNextBit();
        if( Fail ) return false;
        
        if (currentbit == 0 ) head = head-> left;
        else head = head->right ;
      }
  }
  return true;
}

bool Huffdecom::Decode()
{
 if( Fail ) return false;
      ofstream ofs;
  ofs.open(outFileName , ofstream::out | ofstream::ate);
  if( !ofs.is_open() ) return false;
  if( ofs.fail() ) return false;
  if( ofs.eof() ) return false;
    while( MoveToNextBit()) {
  if( !LoadChunk( 1 , ofs) ) return false;
  }
  if( BytePos == ByteSize ) return true;
  if ( !LoadChunk(0 , ofs) ) return false;
return true;
}

bool readData(const char * inFileName, const char * outFileName)
{
  char * Data;
  ifstream ifs;
  //try to open file otherwise return 
  ifs.open( inFileName , ios::binary );
  if( !ifs.is_open() ) return false;

  ifs.seekg( 0 , ifs.end);
  //unsigned long  
  int length = ifs.tellg();
  //seek to end and get the lenght
  ifs.seekg( 0 , ifs.beg );
  if(ifs.fail()) return false;

  Data = new char [length];
  ifs.read( Data , length );
  //read data if fails return false
  if( !ifs.good() || length == 0 ) {
  delete [] Data;
  return false;
  }
  Huffdecom HuffData( length , Data , outFileName);
  if ( !HuffData.GrowTree() ) return false;
  //get the tree and decode
  if ( !HuffData.Decode() ) return false;

return true;
}

bool decompressFile ( const char * inFileName, const char * outFileName )
{
 if( readData( inFileName, outFileName ) ) return true;
  return false;
}

bool compressFile ( const char * inFileName, const char * outFileName )
{
  // keep this dummy implementation (no bonus) or implement the compression (bonus)
  return false;
}

#ifndef __PROGTEST__
bool identicalFiles ( const char * fileName1, const char * fileName2 )
{
  ifstream MyOut( fileName2 , ifstream::binary | ifstream ::ate);
  ifstream RefOut( fileName1 , ifstream::binary | ifstream ::ate);

  if( MyOut.fail() || RefOut.fail() ) return false;
  if (MyOut.tellg() != RefOut.tellg()) return false; //size mismatch
  

  //seek back to beginning and use std::equal to compare contents
  RefOut.seekg(0, std::ifstream::beg);
  MyOut.seekg(0, std::ifstream::beg);
  //iterate
  return std::equal(std::istreambuf_iterator<char>(RefOut.rdbuf()),
                    std::istreambuf_iterator<char>(),
                    std::istreambuf_iterator<char>(MyOut.rdbuf()));
}


int main ( void )
{
   assert ( decompressFile ( "tests/test0.huf", "tempfile" ) );
   assert ( identicalFiles ( "tests/test0.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/test1.huf", "tempfile" ) );
 assert ( identicalFiles ( "tests/test1.orig", "tempfile" ) );

   assert ( decompressFile ( "tests/test2.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/test2.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/test3.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/test3.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/test4.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/test4.orig", "tempfile" ) );

  assert ( ! decompressFile ( "tests/test5.huf", "tempfile" ) );


  // assert ( decompressFile ( "tests/extra0.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra0.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra1.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra1.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra2.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra2.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra3.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra3.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra4.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra4.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra5.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra5.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra6.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra6.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra7.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra7.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra8.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra8.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra9.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra9.orig", "tempfile" ) );

  return 0;
}
#endif /* __PROGTEST__ */
