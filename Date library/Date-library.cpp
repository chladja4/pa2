#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <stdexcept>
using namespace std;
#endif /* __PROGTEST__ */

int RegYear [13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31};
int NotRegYear [13] = { 0 ,31,29,31,30,31,30,31,31,30,31,30,31};
string Arrmonths [12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
bool LeapYear(int year)
{
  if( year % 400 == 0 ) return 1;
  if(year % 100 == 0 ) return 0;
    else if( year % 4 == 0 ) return 1;
      else return 0; 
}

bool Valid(int year, int month , int day)
{
if(LeapYear(year)){
    if(day > NotRegYear[month] || month < 1 || month > 12 || day < 1) return 0;}
else{
  if(day > RegYear[month] || month < 1 || month > 12 || day < 1) return 0;};
return 1;
}

//=================================================================================================
// a dummy exception class, keep this implementation
class InvalidDateException : public invalid_argument
{

  public:
    InvalidDateException ( )
      : invalid_argument ( "invalid date or format" )
    {
    }
};
//=================================================================================================
// date_format manipulator - a dummy implementation. Keep this code unless you implement your
// own working manipulator.
ios_base & ( * date_format ( const char * fmt ) ) ( ios_base & x )
{
  return [] ( ios_base & ios ) -> ios_base & { return ios; };
}
//=================================================================================================
class CDate
{
private:
  int year;
  int month;
  int day;
  public:
  CDate(int year, int month, int day);
  //operators
  friend ostream & operator << (ostream & out , const CDate & a);
  friend istream & operator >> (istream & in , CDate & a);
  friend CDate operator + (const CDate & a, int add );
  friend CDate operator + (int add, const CDate & a );
  friend CDate operator - (const CDate & a, int add );
  friend CDate operator - (int add ,const CDate & a);
  friend int operator - (const CDate & a , const CDate & b );
  friend bool operator == (const CDate & a , const CDate & b );
  friend bool operator != (const CDate & a , const CDate & b );
  friend bool operator < (const CDate & a , const CDate & b );
  friend bool operator <= (const CDate & a , const CDate & b );
  friend bool operator > (const CDate & a , const CDate & b );
  friend bool operator >= (const CDate & a , const CDate & b );
  CDate & operator += ( const int a )
  {return { * this = * this + a }; };

  CDate & operator ++ (void)
  {return *this += 1 ;};

  CDate & operator -= ( const int a )
  { return { * this = * this - a }; }

  CDate operator ++ (int)
  { auto old = *this;
    *this += 1 ;
    return old;};
  
  CDate & operator -- (void)
  {return *this -= 1 ;};

  CDate operator -- (int)
  { auto old = *this;
    *this -= 1 ;
    return old;};
};

CDate::CDate (int year, int month, int day) : year(year), month(month), day(day)
{
  if( !Valid(year,month,day ) ) throw invalid_argument("Nespravny vstup!");
}

ostream & operator << (ostream & out , const CDate & a)
{
  out << a.year << "-";
  if(a.month < 10 ) out << "0";
  out << a.month << "-";
  if( a.day < 10 ) out << "0";
  out << a.day;
  return out;
}

istream & operator >> (istream & in , CDate & a)
{
char sep1,sep2;
int y,m,d;
if ( !( in >> y >> sep1 >> m >> sep2 >> d ) || (sep1 != '-') || (sep2 != '-') || (!Valid(y,m,d)) ){
  in.setstate(ios::failbit);
  return in;}

a.year = y;
a.month = m;
a.day = d;
return in;
}


CDate operator + (const CDate & a , int add)
{
  if(add < 0 )  return a - abs(add);
  stringstream buffer;
  string month,junk;
  int y = 0, m = 0 , d = 0 ;
  tm time1 = {0};
  time1.tm_mday = a.day ;
  time1.tm_mon = a.month - 1  ;
  time1.tm_year =  a.year -1900; 
time_t time = mktime(&time1);
time = time + add * 60*60*24;
char * result = ctime(&time);
buffer << result << endl;
buffer >> junk >> month >> d >> junk >> y ;
for(int i = 0 ; i < 12 ; i ++)
  if(month == Arrmonths[i]){
  m = i+1;
  break;}
return {y,m,d};
}

CDate operator + (int add, const CDate & a ){
    return a + add;}

CDate operator - (const CDate & a, int add )
  {
    if(add < 0 ) return a + abs(add);
 stringstream buffer;
  string month,junk;
  int y = 0, m = 0 , d = 0 ;
  tm time1 = {0};
  time1.tm_mday = a.day ;
  time1.tm_mon = a.month - 1  ;
  time1.tm_year =  a.year -1900; 
time_t time = mktime(&time1);
time = time - add * 60*60*24;
char * result = ctime(&time);
buffer << result;
buffer >> junk >> month >> d >> junk >> y ;
for(int i = 0 ; i < 12 ; i ++)
  if(month == Arrmonths[i]){
  m = i+1;
  break;}
return {y,m,d};
}

CDate operator - (int add ,const CDate & a){
  return a + add;
}

int operator - (const CDate & a , const CDate & b )
{
  tm time1 = {0};
  time1.tm_mday = a.day ;
  time1.tm_mon = a.month - 1  ;
  time1.tm_year =  a.year -1900; 
  tm time2 = {0};
  time2.tm_mday = b.day ;
  time2.tm_mon = b.month - 1  ;
  time2.tm_year =  b.year -1900; 
return difftime ( mktime(&time1) , mktime(&time2) ) / (60*60*24) ;
}

bool operator == (const CDate & a , const CDate & b )
{
  if(a.year == b .year && a.month == b.month && a.day == b.day) return true;
  return false;
}

bool operator != (const CDate & a , const CDate & b )
  {
    return !(a==b);
  }

bool operator < (const CDate & a , const CDate & b ){
  if(a . year < b . year) return true;
  else if(a . year == b . year && a . month < b . month ) return true;
  else if(a . year == b . year && a . month ==  b . month && a . day < b . day ) return true;
  return false;
}

bool operator <= (const CDate & a , const CDate & b ){
  if(a == b) return true;
  if(a . year < b . year) return true;
  else if(a . year == b . year && a . month < b . month ) return true;
  else if(a . year == b . year && a . month ==  b . month && a . day < b . day ) return true;
  return false;
}

bool operator > (const CDate & a , const CDate & b ){
  if ( ! (a <= b) ) return true;
  return false;
}

bool operator >= (const CDate & a , const CDate & b )
{
if ( ! (a < b) ) return true;
return false;
}

#ifndef __PROGTEST__
int main ( void )
{
  ostringstream oss;
  istringstream iss;


  CDate a ( 2000, 1, 2 );
  CDate b ( 2010, 2, 3 );
  CDate c ( 2004, 2, 10 );
  oss . str ("");
  oss << a;
  assert ( oss . str () == "2000-01-02" );
  oss . str ("");
  oss << b;
  assert ( oss . str () == "2010-02-03" );
  oss . str ("");
  oss << c;
  assert ( oss . str () == "2004-02-10" );
  a = a + 1500;
  oss . str ("");
  oss << a;
  assert ( oss . str () == "2004-02-10" );
  b = b - 2000;
  oss . str ("");
  oss << b;
  assert ( oss . str () == "2004-08-13" );
  assert ( b - a == 185 );
  assert ( ( b == a ) == false );
  assert ( ( b != a ) == true );
  assert ( ( b <= a ) == false );
  assert ( ( b < a ) == false );
  assert ( ( b >= a ) == true );
  assert ( ( b > a ) == true );
  assert ( ( c == a ) == true );
  assert ( ( c != a ) == false );
  assert ( ( c <= a ) == true );
  assert ( ( c < a ) == false );
  assert ( ( c >= a ) == true );
  assert ( ( c > a ) == false );
  a = ++c;
  oss . str ( "" );
  oss << a << " " << c;
  assert ( oss . str () == "2004-02-11 2004-02-11" );
  a = --c;
  oss . str ( "" );
  oss << a << " " << c;
  assert ( oss . str () == "2004-02-10 2004-02-10" );
  a = c++;
  oss . str ( "" );
  oss << a << " " << c;
  assert ( oss . str () == "2004-02-10 2004-02-11" );
  a = c--;
  oss . str ( "" );
  oss << a << " " << c;
  assert ( oss . str () == "2004-02-11 2004-02-10" );
  iss . clear ();
  iss . str ( "2015-09-03" );
  assert ( ( iss >> a ) );
  oss . str ("");
  oss << a;
  assert ( oss . str () == "2015-09-03" );
  a = a + 70;
  oss . str ("");
  oss << a;
  assert ( oss . str () == "2015-11-12" );

  CDate d ( 2000, 1, 1 );
  try
  {
    CDate e ( 2000, 32, 1 );
    assert ( "No exception thrown!" == nullptr );
  }
  catch ( ... )
  {
  }
  iss . clear ();
  iss . str ( "2000-12-33" );
  assert ( ! ( iss >> d ) );
  oss . str ("");
  oss << d;
  assert ( oss . str () == "2000-01-01" );
  iss . clear ();
  iss . str ( "2000-11-31" );
  assert ( ! ( iss >> d ) );
  oss . str ("");
  oss << d;
  assert ( oss . str () == "2000-01-01" );
  iss . clear ();
  iss . str ( "2000-02-29" );
  assert ( ( iss >> d ) );
  oss . str ("");
  oss << d;
  assert ( oss . str () == "2000-02-29" );
  iss . clear ();
  iss . str ( "2001-02-29" );
  assert ( ! ( iss >> d ) );
  oss . str ("");
  oss << d;
  assert ( oss . str () == "2000-02-29" );


  // //-----------------------------------------------------------------------------
  // // bonus test examples
  // //-----------------------------------------------------------------------------
  // CDate f ( 2000, 5, 12 );
  // oss . str ("");
  // oss << f;
  // assert ( oss . str () == "2000-05-12" );
  // oss . str ("");
  // oss << date_format ( "%Y/%m/%d" ) << f;
  // assert ( oss . str () == "2000/05/12" );
  // oss . str ("");
  // oss << date_format ( "%d.%m.%Y" ) << f;
  // assert ( oss . str () == "12.05.2000" );
  // oss . str ("");
  // oss << date_format ( "%m/%d/%Y" ) << f;
  // assert ( oss . str () == "05/12/2000" );
  // oss . str ("");
  // oss << date_format ( "%Y%m%d" ) << f;
  // assert ( oss . str () == "20000512" );
  // oss . str ("");
  // oss << date_format ( "hello kitty" ) << f;
  // assert ( oss . str () == "hello kitty" );
  // oss . str ("");
  // oss << date_format ( "%d%d%d%d%d%d%m%m%m%Y%Y%Y%%%%%%%%%%" ) << f;
  // assert ( oss . str () == "121212121212050505200020002000%%%%%" );
  // oss . str ("");
  // oss << date_format ( "%Y-%m-%d" ) << f;
  // assert ( oss . str () == "2000-05-12" );
  // iss . clear ();
  // iss . str ( "2001-01-1" );
  // assert ( ! ( iss >> f ) );
  // oss . str ("");
  // oss << f;
  // assert ( oss . str () == "2000-05-12" );
  // iss . clear ();
  // iss . str ( "2001-1-01" );
  // assert ( ! ( iss >> f ) );
  // oss . str ("");
  // oss << f;
  // assert ( oss . str () == "2000-05-12" );
  // iss . clear ();
  // iss . str ( "2001-001-01" );
  // assert ( ! ( iss >> f ) );
  // oss . str ("");
  // oss << f;
  // assert ( oss . str () == "2000-05-12" );
  // iss . clear ();
  // iss . str ( "2001-01-02" );
  // assert ( ( iss >> date_format ( "%Y-%m-%d" ) >> f ) );
  // oss . str ("");
  // oss << f;
  // assert ( oss . str () == "2001-01-02" );
  // iss . clear ();
  // iss . str ( "05.06.2003" );
  // assert ( ( iss >> date_format ( "%d.%m.%Y" ) >> f ) );
  // oss . str ("");
  // oss << f;
  // assert ( oss . str () == "2003-06-05" );
  // iss . clear ();
  // iss . str ( "07/08/2004" );
  // assert ( ( iss >> date_format ( "%m/%d/%Y" ) >> f ) );
  // oss . str ("");
  // oss << f;
  // assert ( oss . str () == "2004-07-08" );
  // iss . clear ();
  // iss . str ( "2002*03*04" );
  // assert ( ( iss >> date_format ( "%Y*%m*%d" ) >> f ) );
  // oss . str ("");
  // oss << f;
  // assert ( oss . str () == "2002-03-04" );
  // iss . clear ();
  // iss . str ( "C++09format10PA22006rulez" );
  // assert ( ( iss >> date_format ( "C++%mformat%dPA2%Yrulez" ) >> f ) );
  // oss . str ("");
  // oss << f;
  // assert ( oss . str () == "2006-09-10" );
  // iss . clear ();
  // iss . str ( "%12%13%2010%" );
  // assert ( ( iss >> date_format ( "%%%m%%%d%%%Y%%" ) >> f ) );
  // oss . str ("");
  // oss << f;
  // assert ( oss . str () == "2010-12-13" );

  // CDate g ( 2000, 6, 8 );
  // iss . clear ();
  // iss . str ( "2001-11-33" );
  // assert ( ! ( iss >> date_format ( "%Y-%m-%d" ) >> g ) );
  // oss . str ("");
  // oss << g;
  // assert ( oss . str () == "2000-06-08" );
  // iss . clear ();
  // iss . str ( "29.02.2003" );
  // assert ( ! ( iss >> date_format ( "%d.%m.%Y" ) >> g ) );
  // oss . str ("");
  // oss << g;
  // assert ( oss . str () == "2000-06-08" );
  // iss . clear ();
  // iss . str ( "14/02/2004" );
  // assert ( ! ( iss >> date_format ( "%m/%d/%Y" ) >> g ) );
  // oss . str ("");
  // oss << g;
  // assert ( oss . str () == "2000-06-08" );
  // iss . clear ();
  // iss . str ( "2002-03" );
  // assert ( ! ( iss >> date_format ( "%Y-%m" ) >> g ) );
  // oss . str ("");
  // oss << g;
  // assert ( oss . str () == "2000-06-08" );
  // iss . clear ();
  // iss . str ( "hello kitty" );
  // assert ( ! ( iss >> date_format ( "hello kitty" ) >> g ) );
  // oss . str ("");
  // oss << g;
  // assert ( oss . str () == "2000-06-08" );
  // iss . clear ();
  // iss . str ( "2005-07-12-07" );
  // assert ( ! ( iss >> date_format ( "%Y-%m-%d-%m" ) >> g ) );
  // oss . str ("");
  // oss << g;
  // assert ( oss . str () == "2000-06-08" );
  // iss . clear ();
  // iss . str ( "20000101" );
  // assert ( ( iss >> date_format ( "%Y%m%d" ) >> g ) );
  // oss . str ("");
  // oss << g;
  // assert ( oss . str () == "2000-01-01" );

  return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */
