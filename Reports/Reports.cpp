#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <cassert>
#include <iostream> 
#include <iomanip> 
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */


string Transform(const string & ToTransform){
  string Element = ToTransform;
   transform(Element.begin(), Element.end(), Element.begin(), [](unsigned char c) -> unsigned char { return tolower(c);});
  return Element;
}

struct Company
  {
    string name;
    string addr;
    string mix;
    string taxID;
    unsigned int  InvoicesSum;
    Company( const string & name , const string & addr, const string &taxID) : name(name), addr(addr), taxID(taxID), InvoicesSum(0){
      mix = Transform(name) + " ; " + Transform(addr);
    }
  };


class CVATRegister
{
  public:
                  CVATRegister   ( void ){};
                  ~CVATRegister  ( void ){
                    for(auto & El : CompaniesID)
                    delete El;
                  };
    bool          newCompany     ( const string    & name,
                                   const string    & addr,
                                   const string    & taxID );
    bool          cancelCompany  ( const string    & name,
                                   const string    & addr );
    bool          cancelCompany  ( const string    & taxID );
    bool          invoice        ( const string    & taxID,
                                   unsigned int      amount );
    bool          invoice        ( const string    & name,
                                   const string    & addr,
                                   unsigned int      amount );
    bool          audit          ( const string    & name,
                                   const string    & addr,
                                   unsigned int    & sumIncome ) const;
    bool          audit          ( const string    & taxID,
                                   unsigned int    & sumIncome ) const;
    bool          firstCompany   ( string          & name,
                                   string          & addr ) const;
    bool          nextCompany    ( string          & name,
                                   string          & addr ) const;
    unsigned int  medianInvoice  ( void ) const;
    Company *     BSearchID (const string & Searched) const;
    Company *     BSearchName (const string & Searched) const;
    int           BSearchNameIndex (const string & Searched) const;
    int           BSearchIDIndex (const string & Searched) const;
    void          PrintIt   () const ;
  private:
    vector<int> Invoices;
    vector<Company*> CompaniesID;
    vector<Company*> CompaniesName;
};

bool compID(const Company * a , const Company * b)
{
  if(a->taxID < b->taxID ) return true;
  return false;
}

bool compName(const Company * a , const Company * b)
{
  if(a->mix < b->mix ) return true;
  return false;
}

bool CVATRegister::newCompany ( const string & name, const string & addr, const string & taxID )
{
  string nameL = Transform(name);
  string addrL =  Transform (addr);
  for(auto & El : CompaniesID){
  string Name = Transform(El->name);
  string Addr = Transform(El->addr);
  if( (Name == nameL && Addr == addrL) || El -> taxID == taxID ) return false;
  }

  Company * tmp = new Company(name ,addr ,taxID);
  auto index = lower_bound(CompaniesID.begin(), CompaniesID.end(), tmp , compID);
  CompaniesID.insert(index,tmp);
   index = lower_bound(CompaniesName.begin(), CompaniesName.end(), tmp , compName);
  CompaniesName.insert(index, tmp);
  return true;
}

int CVATRegister::BSearchIDIndex (const string & Searched) const
  {
    int left = 0; 
    int right = CompaniesID.size()-1;
    while(left <= right)
    {
      int mid = left + (right-left)/2;
      if(CompaniesID[mid]->taxID == Searched) return mid;
      else if (CompaniesID[mid]->taxID < Searched ) left = mid + 1;
      else right = mid -1;
    }
    return -1;
  }

Company * CVATRegister::BSearchID (const string & Searched) const
  {
    int left = 0; 
    int right = CompaniesID.size()-1;
    while(left <= right)
    {
      int mid = left + (right-left)/2;
      if(CompaniesID[mid]->taxID == Searched) return CompaniesID[mid];
      else if (CompaniesID[mid]->taxID < Searched ) left = mid + 1;
      else right = mid -1;
    }
    return nullptr;
  }

  Company * CVATRegister::BSearchName (const string & Searched) const
  {
    int left = 0; 
    int right = CompaniesName.size()-1;
    while(left <= right)
    {
      int mid = left + (right-left)/2;
      if(CompaniesName[mid]->mix == Searched) return CompaniesName[mid];
      else if (CompaniesName[mid]->mix < Searched ) left = mid + 1;
      else right = mid -1;
    }
    return nullptr;
  }

  int CVATRegister::BSearchNameIndex (const string & Searched) const
  {
    int left = 0; 
    int right = CompaniesName.size()-1;
    while(left <= right)
    {
      int mid = left + (right-left)/2;
      if(CompaniesName[mid]->mix == Searched) return mid;
      else if (CompaniesName[mid]->mix < Searched ) left = mid + 1;
      else right = mid -1;
    }
    return -1;
  }

bool CVATRegister::invoice ( const string & taxID, unsigned int amount )
  {
    auto Company = BSearchID(taxID);
    if(Company == nullptr ) return false;
    Company->InvoicesSum += amount;
    auto index = lower_bound(Invoices.begin(), Invoices.end(), amount);
    Invoices.insert(index, amount);
    return true;
  }

  bool CVATRegister::invoice ( const string    & name, const string    & addr, unsigned int amount )
  {
    auto Company = BSearchName(Transform(name) + " ; " + Transform(addr));
   if( Company == nullptr )return false;
   Company->InvoicesSum += amount;
   auto index = lower_bound(Invoices.begin(), Invoices.end(), amount);
    Invoices.insert(index, amount);
    return true;
  }

void CVATRegister::PrintIt() const
  {
    cout << CompaniesID.size() << endl;
    cout << CompaniesName.size() << endl;
  }

unsigned int  CVATRegister::medianInvoice  ( void ) const
  {
    if( !Invoices.size() ) return false;
    int index = Invoices.size()/2;
    return Invoices[index];
  }

bool CVATRegister::audit ( const string & taxID, unsigned int & sumIncome ) const
  {
    auto Company = BSearchID(taxID);
    if ( Company == nullptr ) return false;
    sumIncome = Company->InvoicesSum;
    return true;
  }

bool CVATRegister::audit ( const string & name, const string & addr, unsigned int & sumIncome ) const
  {
    auto Company = BSearchName( Transform(name) + " ; " + Transform(addr));
    if ( Company == nullptr ) return false;
    sumIncome = Company->InvoicesSum;
    return true;
  }

  bool CVATRegister::firstCompany ( string & name, string  & addr ) const
    {
      if( !CompaniesName.size() ) return false;
      name = CompaniesName[0] -> name;
      addr = CompaniesName[0] -> addr;
      return true;
    }

bool CVATRegister::nextCompany ( string & name, string & addr ) const
  {
   size_t index = BSearchNameIndex ( Transform(name) + " ; " + Transform(addr)) + 1;
   if(index == CompaniesName.size()) return false;
   if ( index == 0  ) return false;
   name = CompaniesName[index]->name;
   addr = CompaniesName[index]->addr;
   return true;
  } 

bool CVATRegister::cancelCompany ( const string & taxID )
  {
    if( !CompaniesID.size() ) return false;
    int  index = BSearchIDIndex(taxID);
    if( index == -1 ) return false;
    int index2 = BSearchNameIndex(Transform( (CompaniesID[index]->name) ) + " ; " + Transform( (CompaniesID[index]->addr) ) );
    if(index2 == -1 ) return false;
    delete CompaniesID[index];
    CompaniesID.erase(CompaniesID.begin() + index);
    CompaniesName.erase( CompaniesName.begin() + index2 );
    return true;
  }

   bool CVATRegister::cancelCompany  ( const string & name, const string & addr )
   {
    if( !CompaniesID.size() ) return false;
    int index = BSearchNameIndex( Transform(name) + " ; " + Transform(addr) );
    if( index == -1 ) return false;
    int index2 = BSearchIDIndex (CompaniesName[index]->taxID);
    if(index2 == -1 ) return false;
    delete CompaniesID[index2];
    CompaniesID.erase(CompaniesID.begin() + index2);
    CompaniesName.erase( CompaniesName.begin() + index );
    return true;
   }


#ifndef __PROGTEST__
int               main           ( void )
{
  string name, addr;
  unsigned int sumIncome;

  CVATRegister b1;
  assert ( b1 . newCompany ( "ACME", "Thakurova", "666/666" ) );
  assert ( b1 . newCompany ( "ACME", "Kolejni", "666/666/666" ) );
  assert ( b1 . newCompany ( "Dummy", "Thakurova", "123456" ) );


  assert ( b1 . invoice ( "666/666", 2000 ) );
 
  assert ( b1 . medianInvoice () == 2000 );
  assert ( b1 . invoice ( "666/666/666", 3000 ) );
  assert ( b1 . medianInvoice () == 3000 );
  assert ( b1 . invoice ( "123456", 4000 ) );
  assert ( b1 . medianInvoice () == 3000 );
  assert ( b1 . invoice ( "aCmE", "Kolejni", 5000 ) );
  assert ( b1 . medianInvoice () == 4000 );
  assert ( b1 . audit ( "ACME", "Kolejni", sumIncome ) && sumIncome == 8000 );
  assert ( b1 . audit ( "123456", sumIncome ) && sumIncome == 4000 );
  assert ( b1 . firstCompany ( name, addr ) && name == "ACME" && addr == "Kolejni" );
  assert ( b1 . nextCompany ( name, addr ) && name == "ACME" && addr == "Thakurova" );
  assert ( b1 . nextCompany ( name, addr ) && name == "Dummy" && addr == "Thakurova" );
  assert ( ! b1 . nextCompany ( name, addr ) );
  assert ( b1 . cancelCompany ( "ACME", "KoLeJnI" ) );
   assert ( b1 . medianInvoice () == 4000 );
    assert ( b1 . cancelCompany ( "666/666" ) );
   assert ( b1 . medianInvoice () == 4000 );

 assert ( b1 . invoice ( "123456", 100 ) );
  assert ( b1 . medianInvoice () == 3000 );
  assert ( b1 . invoice ( "123456", 300 ) );
  assert ( b1 . medianInvoice () == 3000 );
  assert ( b1 . invoice ( "123456", 200 ) );
  assert ( b1 . medianInvoice () == 2000 );
  assert ( b1 . invoice ( "123456", 230 ) );
  assert ( b1 . medianInvoice () == 2000 );
  assert ( b1 . invoice ( "123456", 830 ) );
  assert ( b1 . medianInvoice () == 830 );
  assert ( b1 . invoice ( "123456", 1830 ) );
  assert ( b1 . medianInvoice () == 1830 );
  assert ( b1 . invoice ( "123456", 2830 ) );
  assert ( b1 . medianInvoice () == 1830 );
  assert ( b1 . invoice ( "123456", 2830 ) );
  assert ( b1 . medianInvoice () == 2000 );
  assert ( b1 . invoice ( "123456", 3200 ) );
  assert ( b1 . medianInvoice () == 2000 );
  assert ( b1 . firstCompany ( name, addr ) && name == "Dummy" && addr == "Thakurova" );
  assert ( ! b1 . nextCompany ( name, addr ) );
  assert ( b1 . cancelCompany ( "123456" ) );
  assert ( ! b1 . firstCompany ( name, addr ) );

  CVATRegister b2;
  assert ( b2 . newCompany ( "ACME", "Kolejni", "abcdef" ) );
  assert ( b2 . newCompany ( "Dummy", "Kolejni", "123456" ) );
  assert ( ! b2 . newCompany ( "AcMe", "kOlEjNi", "1234" ) );
  assert ( b2 . newCompany ( "Dummy", "Thakurova", "ABCDEF" ) );
   assert ( b2 . medianInvoice () == 0 );
   assert ( b2 . invoice ( "ABCDEF", 1000 ) );
   assert ( b2 . medianInvoice () == 1000 );
   assert ( b2 . invoice ( "abcdef", 2000 ) );
   assert ( b2 . medianInvoice () == 2000 );
   assert ( b2 . invoice ( "aCMe", "kOlEjNi", 3000 ) );
   assert ( b2 . medianInvoice () == 2000 );
   assert ( ! b2 . invoice ( "1234567", 100 ) );
   assert ( ! b2 . invoice ( "ACE", "Kolejni", 100 ) );
   assert ( ! b2 . invoice ( "ACME", "Thakurova", 100 ) );
   assert ( ! b2 . audit ( "1234567", sumIncome ) );
   assert ( ! b2 . audit ( "ACE", "Kolejni", sumIncome ) );
   assert ( ! b2 . audit ( "ACME", "Thakurova", sumIncome ) );
   assert ( ! b2 . cancelCompany ( "1234567" ) );
   assert ( ! b2 . cancelCompany ( "ACE", "Kolejni" ) );
   assert ( ! b2 . cancelCompany ( "ACME", "Thakurova" ) );
   assert ( b2 . cancelCompany ( "abcdef" ) );
   assert ( b2 . medianInvoice () == 2000 );
  assert ( ! b2 . cancelCompany ( "abcdef" ) );
  assert ( b2 . newCompany ( "ACME", "Kolejni", "abcdef" ) );
  assert ( b2 . cancelCompany ( "ACME", "Kolejni" ) );
  assert ( ! b2 . cancelCompany ( "ACME", "Kolejni" ) );
  
  return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */
